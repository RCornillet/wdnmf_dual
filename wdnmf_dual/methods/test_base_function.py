# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 13:57:40 2023

@author: rcornill
"""

import numpy as np
import torch as tn
from wdnmf_dual.methods.base_function import *

def test_simplex_prox_mat_projection():
    x = tn.ones(2, 3, dtype=tn.float64)
    x = x/sum(x)
    assert tn.all(x == simplex_prox_mat(x))
    
def test_simplex_prox_mat1(): # La projection se fait selon l2 et non l1 ici!
    x = tn.tensor([[1, 2, 0], [3, 2, 1]], dtype=tn.float64)
    w = tn.tensor([[0., 0.5, 0], [1, 0.5, 1]], dtype=tn.float64)
    assert tn.all(w == simplex_prox_mat(x))
    
def test_simplex_norm_projection():
    x = tn.ones(2, 3, dtype=tn.float64)
    x = x/sum(x)
    assert tn.all(x == simplex_norm(x))
    
def test_simplex_norm1(): # La projection se fait selon l1 ici!
    x = tn.tensor([[1, 2, 0], [3, 2, 1]], dtype=tn.float64)
    w = tn.tensor([[0.25, 0.5, 0], [0.75, 0.5, 1]], dtype=tn.float64)
    assert tn.all(w == simplex_norm(x))

def test_e_mat_vec():
    x = tn.tensor([0.5, 1, 0.1], dtype=tn.float64)
    w = 0.5*np.log(0.5) + 0 + 0.1*np.log(0.1)
    assert tn.all(e_mat(x)==w)

def test_e_mat():
    x = tn.tensor([[0.5, 1, 0.1]], dtype=tn.float64)
    w = 0.5*np.log(0.5) + 0 + 0.1*np.log(0.1)
    assert tn.all(e_mat(x)==w)

def test_e_dual():
    x = tn.tensor([1, 0, 0.5], dtype=tn.float64)
    w = -np.log(np.e + 1 + np.exp(0.5))
    assert tn.all(e_dual(x)==w)

def test_grad_e_dual():
    x = tn.tensor([1, 0, 0.5], dtype=tn.float64)
    w = tn.tensor([np.e, 1, np.exp(0.5)])
    w = w/sum(w)
    assert tn.all(w == grad_e_dual(x))

def test_matK():
    cost = tn.tensor([[0, 1], [2, 0]], dtype=tn.float64)
    ent1 = 1
    w1 = tn.tensor([[-1, -np.e],[-np.exp(2), -1]], dtype=tn.float64)
    assert tn.all(w1 == matK(cost, ent1))
    ent2 = 2
    w2 = tn.tensor([[-0.5, -np.e/2],[-np.exp(2)/2, -0.5]], dtype=tn.float64)
    assert tn.all(w2 == matK(cost, ent2))