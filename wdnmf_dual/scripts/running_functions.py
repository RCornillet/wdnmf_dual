import wdnmf_dual as wd
import torch as tn
from wdnmf_dual.methods.grad_pb_dual import pb_dual, grad1_pb_dual, pb
from wdnmf_dual.methods.base_function import compute_matK, simplex_norm
from wdnmf_dual.methods.Optim_pb import optim_w

n = 3
m = 4
r = 2
rho1 = 0.1

aim = tn.ones(n, m) + tn.rand(n,m)
aim = simplex_norm(aim)

#dictionary = tn.tensor([[0.05, 0.05],[0.9, 0.05], [0.05, 0.9]], dtype=tn.float64)
dictionary = tn.abs(tn.rand(n,2, dtype=tn.float64))
dictionary = simplex_norm(dictionary)

#w = tn.tensor([[0.9, 0.05],[0.05, 0.05], [0.05, 0.9]], dtype=tn.float64)
w = tn.ones(n, r) + tn.rand(n,r)
w = simplex_norm(w)

h = tn.ones(r, m) + tn.rand(r,m)
h = simplex_norm(h)

data = dictionary@h

sigma = [0,1]

reg = 0.1

ent = 0.1

rho2 = 0.1

cost = tn.zeros((n, n), dtype=tn.float64)
for i in range(n):
    for j in range(n):
        cost[i, j] = abs(i-j)

matK = compute_matK(cost, ent)


out = pb_dual(aim, w, rho1, h, data, sigma, dictionary, reg, ent, matK)

out2 = grad1_pb_dual(aim, w, rho1, h, data, ent, matK)

print(w)

print(dictionary)


out3 = optim_w(w, h, data, sigma, dictionary, reg, ent, rho1, rho2, cost)

print(out3)

out4 = pb(w = out3, h = h, sigma = sigma, data = data, dictionary = dictionary, reg = reg, 
     ent = ent, rho1 = rho1, rho2=rho2, cost = cost)

print(out4)